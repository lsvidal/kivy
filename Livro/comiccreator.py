import kivy
kivy.require('1.9.0')

from kivy.app import App
from kivy.lang import Builder
from kivy.uix.anchorlayout import AnchorLayout

# Ele importa o comiccreator.kv automaticamente pelo nome da app
Builder.load_file('toolbox.kv')
Builder.load_file('generaloptions.kv')
Builder.load_file('statusbar.kv')
Builder.load_file('drawingspace.kv')

class ComicCreator(AnchorLayout):
  pass

class ComicCreatorApp(App):
  def build(self):
    return ComicCreator()

if __name__ == '__main__':
  ComicCreatorApp().run()
