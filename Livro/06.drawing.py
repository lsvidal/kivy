'''
Executar com parâmetro --size=500x300 por que as posições são fixas
'''
import kivy
kivy.require('1.9.0')

from kivy.app import App
from kivy.uix.relativelayout import RelativeLayout

class DrawingSpace(RelativeLayout):
  pass

class DrawingApp(App):
  def build(self):
    return DrawingSpace()

if __name__ == '__main__':
  DrawingApp().run()